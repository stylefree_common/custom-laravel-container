<?php

namespace Container\Auth;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

/**
 * User Providerクラス
 *
 * @author tatsuki.kubota
 */
class CustomUserProvider implements UserProvider
{
    /**
     * The hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * Data Access Object
     *
     * @var \Container\Support\Dao\Users
     */
    private $dao;

    /**
     * Notification object class name
     *
     * @var string
     */
    private $notification;

    /**
     * Create a new user provider.
     *
     * @param HasherContract $hasher
     * @param string         $dao
     * @param string         $notification
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @return void
     */
    public function __construct(HasherContract $hasher, $dao, $notification)
    {
        $this->hasher       = $hasher;
        $this->dao          = new $dao;
        $this->notification = $notification;
    }

    /**
     * Retrieve a user by their unique identifier.
     * (ユニークIDより、ユーザー情報を取得します)
     *
     * @param  mixed  $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->getGenericUser($this->dao->selectId($identifier));
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     * (ユニークID + "remember me"トークンより、ユーザ情報を取得します )
     *
     * @param  mixed   $identifier
     * @param  string  $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        try {
            return $this->getGenericUser($this->dao->selectId($identifier));
        } catch (\Exception $e) {
            unset($e);
            return null;
        }
    }

    /**
     * Update the "remember me" token for the given user in storage.
     * (指定したユーザの"remember me"トークンを更新します)
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  string  $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->dao->updateRememberToken($user->getAuthIdentifier(), $token);
    }

    /**
     * Retrieve a user by the given credentials.
     * (資格情報より、ユーザ情報を取得します)
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $response = null;
        if (isset ($credentials['email'])) {
            $response = $this->dao->selectEmail($credentials['email']);
        } elseif (isset($credentials['api_token'])) {
            $response = $this->dao->selectApiToken($credentials['api_token']);
        } else {
            return null;
        }
        return $this->getGenericUser($response);
    }

    /**
     * Validate a user against the given credentials.
     * (資格情報より、ユーザ情報を検証します)
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->hasher->check(
            $credentials['password'], $user->getAuthPassword()
        );
    }

    /**
     * Get the generic user.
     *
     * @param  array $users
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getGenericUser($users)
    {
        if (isset($users[0]) === false) {
            return null;
        }
        return new CustomUser($users[0], $this->notification);
    }
}
