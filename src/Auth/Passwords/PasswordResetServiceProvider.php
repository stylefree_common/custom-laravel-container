<?php

namespace Container\Auth\Passwords;

use Illuminate\Auth\Passwords\PasswordResetServiceProvider as ServiceProvider;

/**
 * パスワードリセット サービスプロバイダ
 *
 * @see \Illuminate\Auth\Passwords\PasswordResetServiceProvider
 *
 * @author tatsuki.kubota
 */
class PasswordResetServiceProvider extends ServiceProvider
{
    /**
     * PasswordBrokerをサービスコンテナに登録
     * Laravelのものではなく独自のクラスを利用する
     */
    protected function registerPasswordBroker()
    {
        $this->app->singleton('auth.password', function ($app) {
            return new CustomPasswordBrokerManager($app);
        });
        $this->app->bind('auth.password.broker', function ($app) {
            return $app->make('auth.password')->broker();
        });
    }
}
