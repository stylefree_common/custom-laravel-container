<?php

namespace Container\Auth\Passwords;

use Illuminate\Auth\Passwords\PasswordBrokerManager;
use Illuminate\Support\Str;

/**
 * パスワードブローカー マネージャ
 *
 * @see \Illuminate\Auth\Passwords\PasswordBrokerManager
 *
 * @author tatsuki.kubota
 */
class CustomPasswordBrokerManager extends PasswordBrokerManager
{
    /**
     * Create a token repository instance based on the given configuration.
     *
     * @param  array  $config
     * @return \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    protected function createTokenRepository(array $config)
    {
        $key = $this->app['config']['app.key'];
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        return new CustomTokenRepository($this->app['hash'], $key, $config['dao'], $config['expire']);
    }
}
