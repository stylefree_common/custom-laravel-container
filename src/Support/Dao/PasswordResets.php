<?php

namespace Container\Support\Dao;

/**
 * password_resets DAOクラス
 *
 * @author tatsuki.kubota
 */
interface PasswordResets
{
    /**
     * 対象のリセットトークンを削除します。
     *
     * @param  string $email メールアドレス
     * @return array         クエリ結果
     */
    public static function selectEmail($email);

    /**
     * 対象のリセットトークンを削除します。
     *
     * @param  string $email メールアドレス
     * @return int           クエリ結果
     */
    public static function deleteExisting($email);

    /**
     * 期限切れのリセットトークンを削除します。
     *
     * @param  string $expiredAt 有効期限
     * @return int               クエリ結果
     */
    public static function deleteExpired($expiredAt);

    /**
     * リセットトークンを作成します。
     *
     * @param  string $email     メールアドレス
     * @param  string $token     一時記憶トークン
     * @param  string $createdAt 作成日時
     * @return int               クエリ結果
     */
    public static function insertResetToken($email, $token, $createdAt);
}
