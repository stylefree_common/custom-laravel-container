<?php

namespace Container\Support\Dao;

/**
 * users DAOクラス
 *
 * @author tatsuki.kubota
 */
interface Users
{
    /**
     * ユーザIDよりユーザ情報を取得します。
     *
     * @param  string $id ユーザID
     * @return array      クエリ結果
     */
    public static function selectId($id);

    /**
     * メールアドレスよりユーザ情報を取得します。
     *
     * @param  string $email メールアドレス
     * @return array         クエリ結果
     */
    public static function selectEmail($email);

    /**
     * APIトークンよりユーザ情報を取得します。
     *
     * @param  string $apiToken APIトークン
     * @return array            クエリ結果
     */
    public static function selectApiToken($apiToken);

    /**
     * 一時記憶トークンよりユーザ情報を取得します。
     *
     * @param  string $id            ユーザID
     * @param  string $rememberToken 一時記憶トークン
     * @return array                 クエリ結果
     */
    public static function selectRememberToken($id, $rememberToken);

    /**
     * 一時記憶トークンを更新します。
     *
     * @param  string $id            ユーザID
     * @param  string $rememberToken 一時記憶トークン
     * @return int
     */
    public static function updateRememberToken($id, $rememberToken);

    /**
     * パスワードを更新します。
     *
     * @param  string $id            ユーザID
     * @param  string $password      パスワード
     * @param  string $rememberToken 一時記憶トークン
     * @return int
     */
    public static function updatePassword($id, $password, $rememberToken);

    /**
     * ユーザを登録します。
     *
     * @param  string $name
     * @param  string $email
     * @param  string $password
     * @param  string $apiToken
     * @return int
     */
    public static function insertUsers($name, $email, $password, $apiToken);
}
