<?php

namespace Container\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Container\Database\PdoMysql
 *
 * @author tatsuki.kubota
 */
class DB extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'db';
    }
}
