<?php

namespace Container\Support\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Container\Log\ExtensionLog
 *
 * @author tatsuki.kubota
 */
class Log extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ExtensionLog';
    }
}
