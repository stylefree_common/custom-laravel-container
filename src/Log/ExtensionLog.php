<?php

namespace Container\Log;

/**
 * 拡張Logクラス
 * monolog(Laravelコンテナ利用)のラッパークラス
 *
 * @author tatsuki.kubota
 */
class ExtensionLog
{
    // ログ出力定義
    private static $contextValue = [];

    // Laravelログコンテナ
    /** @var \Illuminate\Log\Writer $logInstance **/
    private static $logInstance = null;

    // 1行ログ出力限界
    private static $splitLength = null;

    /**
     * (拡張メソッド) context固定出力値を定義します。
     *
     * @param array $elementMap
     */
    public static function setValue($elementMap = [])
    {
        self::$contextValue = array_merge(self::$contextValue, $elementMap);
    }

    /**
     * Log an emergency message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function emergency($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log an alert message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function alert($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log a critical message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function critical($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log an error message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function error($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log a warning message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function warning($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log a notice to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function notice($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log an informational message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log a debug message to the logs.
     *
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function debug($message, array $context = [])
    {
        $this->staticWriteLog(__FUNCTION__, $message, $context);
    }

    /**
     * Log a message to the logs.
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message, array $context = [])
    {
        $this->staticWriteLog($level, $message, $context);
    }

    /**
     * Dynamically pass log calls into the writer.
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    public function write($level, $message, array $context = [])
    {
        $this->staticWriteLog($level, $message, $context);
    }

    /**
     * Write a message to Monolog.
     *
     * @param  string  $level
     * @param  string  $message
     * @param  array  $context
     * @return void
     */
    protected function staticWriteLog($level, $message, $context)
    {
        if (self::$logInstance === null) {
            self::$logInstance = app('log');
            self::$splitLength = (int)env('APP_LOG_LENGTH', 0);
        }
        if (self::$splitLength === 0) {
            self::$logInstance->write($level, $message, array_merge($context, self::$contextValue));
        } else {
            $messageLength = mb_strlen($message, 'UTF-8');
            for ($i = 0; $i < $messageLength; $i += self::$splitLength) {
                self::$logInstance->write($level, mb_substr($message, $i, self::$splitLength), array_merge($context, self::$contextValue));
            }
        }
    }
}
