<?php

namespace Container\Database;

use Exception;
use Illuminate\Database\ConnectionResolverInterface;
use Container\Database\TimeoutException;

/**
 * DBオペレーションクラス
 * PDO操作とPDOインスタンスの管理を行う。
 *
 * @author tatsuki.kubota
 * @package Container\Database
 */
class PdoMysql implements ConnectionResolverInterface
{
    // 定数
    const SQL_TYPE_SELECT  = 'select';
    const SQL_TYPE_INSERT  = 'insert';
    const SQL_TYPE_UPDATE  = 'update';
    const SQL_TYPE_DELETE  = 'delete';
    const QUESTION_SYMBOL  = '?';
    const TAG_SYMBOL       = ':';
    const SPLIT_SYMBOL     = ',';

    // クラス変数
    private static $pdoList      = [];
    private static $timeout      = null;
    private static $connectName  = null;

    /**
     * Get a database connection instance.
     *
     * @param  string  $name
     * @return \Illuminate\Database\ConnectionInterface
     */
    public function connection($name = null)
    {
        if (!empty($name)) {
            self::connect($name);
        }
    }

    /**
     * Get the default connection name.
     *
     * @return string
     */
    public function getDefaultConnection()
    {
        return config('database.default', 'mysql');
    }

    /**
     * Set the default connection name.
     *
     * @param  string  $name
     * @return void
     */
    public function setDefaultConnection($name)
    {
        if (!empty($name)) {
            self::connect($name);
        }
    }

    /**
     * SELECT文を実行します。
     *
     * @param  string $sql     SQLクエリ
     * @param  array $values   プリペアードステート配列
     * @return array | integer レコード | 更新件数
     */
    public static function select($sql, $values = [])
    {
        return self::exec(__FUNCTION__, $sql, $values);
    }

    /**
     * INSERT文を実行します。
     *
     * @param  string $sql     SQLクエリ
     * @param  array $values   プリペアードステート配列
     * @return array | integer レコード | 更新件数
     */
    public static function insert($sql, $values = [])
    {
        return self::exec(__FUNCTION__, $sql, $values);
    }

    /**
     * UPDATE文を実行します。
     *
     * @param  string $sql     SQLクエリ
     * @param  array $values   プリペアードステート配列
     * @return array | integer レコード | 更新件数
     */
    public static function update($sql, $values = [])
    {
        return self::exec(__FUNCTION__, $sql, $values);
    }

    /**
     * DELETE文を実行します。
     *
     * @param  string $sql     SQLクエリ
     * @param  array $values   プリペアードステート配列
     * @return array | integer レコード | 更新件数
     */
    public static function delete($sql, $values = [])
    {
        return self::exec(__FUNCTION__, $sql, $values);
    }

    /**
     * トランザクションを開始します。
     *
     * @return $this
     */
    public function transaction()
    {
        /** @var \PDO|string|null $pdo */
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO === false) {
            // まだPDOインスタンスが格納されていない場合
            // PDOインスタンス作成
            $pdo = self::createPdoInstance($pdo);
        } elseif ($pdo->inTransaction()) {
            // 既にPDOインスタンスが格納されている + トランザクション中である場合
            $pdo->rollBack();
        }
        $pdo->beginTransaction();
        self::$pdoList[self::$connectName] = $pdo;
        return $this;
    }

    /**
     * トランザクションをコミットします。
     *
     * @return $this
     */
    public function commit()
    {
        /** @var \PDO|string|null $pdo */
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO && $pdo->inTransaction()) {
            $pdo->commit();
            self::$pdoList[self::$connectName] = $pdo;
        }
        return $this;
    }

    /**
     * トランザクションをロールバックします。
     *
     * @return $this
     */
    public function rollback()
    {
        /** @var \PDO|string|null $pdo */
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO && $pdo->inTransaction()) {
            $pdo->rollBack();
            self::$pdoList[self::$connectName] = $pdo;
        }
        return $this;
    }

    /**
     * データベース接続先を設定します
     *
     * @param string $connect 接続情報設定値名 (database.php - connections定義)
     * @return $this
     */
    public function connect($connect = null)
    {
        // 接続情報の取得
        if (empty($connect) === false || isset(config('database.connections')[$connect])) {
            self::$connectName = $connect;
        } else {
            throw new Exception('DB接続設定エラー : 接続定義がありません');
        }

        // インスタンス確認
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO === false) {
            // まだPDOインスタンスが格納されていない場合
            // 接続DB設定名格納 (実行時インスタンス生成)
            self::$pdoList[self::$connectName] = self::$connectName;
        } elseif ($pdo->inTransaction()) {
            // 既にPDOインスタンスが格納されている + トランザクション中である場合
            $pdo->rollBack();
            self::$pdoList[self::$connectName] = $pdo;
        }

        return $this;
    }

    /**
     * 接続タイムアウト(ミリ秒)を設定します。
     * (※ タイムアウト発生時、TimeoutExceptionをthrowします。)
     *
     * @param int|null $timeout タイムアウト時間 (ミリ秒)
     * @return $this
     */
    public function timeout($timeout)
    {
        self::$timeout = $timeout;
        return $this;
    }

    /**
     * 一つ前の操作で登録したデータIDを取得します。
     *
     * @return string
     */
    public function getLastInsertId()
    {
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO === false) {
            return 0;
        }
        return $pdo->lastInsertId();
    }

    /**
     * フィールド変数に格納しているPDOインスタンスを取得する。
     *
     * @return \PDO|string|null
     */
    private static function getInstance()
    {
        if (isset(self::$pdoList[self::$connectName])) {
            return self::$pdoList[self::$connectName];
        }
        return config('database.default', 'mysql');
    }

    /**
     * DBセッションを開始します。
     *
     * @return \PDO
     */
    private static function createPdoInstance($connectName)
    {
        // 接続設定の取得
        $setting = config('database.connections.' . $connectName, null);
        if (empty($setting)) {
            throw new Exception('DB接続設定エラー : 接続ファイルがありません');
        }

        // DSN (Data Source Name)の生成
        $dsn = $setting['driver']
                . ':host=' . $setting['host']
                . (isset($setting['port']) ? ';port=' . $setting['port'] : '')
                . ';dbname=' . $setting['database']
                . ';sslmode=require';
        try {
            // PDOインスタンス作成
            $pdo = new \PDO($dsn, $setting['username'], $setting['password'], [
                \PDO::ATTR_CURSOR                    => \PDO::CURSOR_SCROLL,
                \PDO::ATTR_ERRMODE                   => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE        => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES          => true,
                \PDO::ATTR_PERSISTENT                => false,
                \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY  => false,
                \PDO::MYSQL_ATTR_INIT_COMMAND        => 'SET NAMES ' . $setting['charset'],
            ]);
            // タイムアウト未定義時 30分
            $timeout = 1800;
            if (self::$timeout === null && isset($setting['timeout'])) {
                $timeout = $setting['timeout'];
            } elseif (self::$timeout !== null) {
                $timeout = self::$timeout;
            }
            $pdo->query('SET SESSION wait_timeout=' . $timeout);
            return $pdo;
        } catch (\PDOException $e) {
            throw new Exception('DB接続設定エラー : ' . $e->getMessage());
        }
    }

    /**
     * SQLを実行します。
     *
     * @param  string $sqlType SQL実行種別
     * @param  string $sql     SQLクエリ
     * @param  array $values   プリペアードステート配列
     * @return array | integer レコード | 更新件数
     * @throws TimeoutException
     */
    private static function exec($sqlType, $sql, $values = [])
    {
        // インスタンス取得
        $pdo = self::getInstance();
        if ($pdo instanceof \PDO === false) {
            $pdo = self::createPdoInstance($pdo);
            self::$pdoList[self::$connectName] = $pdo;
        }

        // クエリ加工
        self::formatSql($sql);

        // プレースホルダの確認
        if (empty($values) === false) {
            // タグの並び替え
            if (self::isAssoc($values)) {
                // SQL・プリペアードステート配列の書き換え
                self::rewriteSqlForTag($sql, $values);
            } else {
                // SQL・プリペアードステート配列の書き換え
                self::rewriteSqlForSymbol($sql, $values);
            }
        }

        try {
            // クエリ実行
            $startTime = microtime(true);
            /** @var \PDOStatement $statement */
            $statement = null;
            if (empty($values)) {
                $statement = $pdo->query($sql);
            } else {
                $statement = $pdo->prepare($sql);
                $statement->execute($values);
            }
            $endTime = microtime(true);

            $response = null;
            if (self::isWriteSql($sqlType)) {
                // 更新数取得 (INSERT・UPDATE・DELETE)
                $response = $recordCount = $statement->rowCount();
            } else {
                // 結果取得 (SELECT)
                $response = $statement->fetchAll();
                $recordCount = count($response);
            }
            // ログ出力
            foreach ($values as &$value) {
                $value = '\'' . $value . '\'';
            }
            \Log::info(sprintf('SQL実行処理 (処理件数: %s件, 処理時間: %s秒) SQL: %s',
                $recordCount, round($endTime - $startTime, 3), str_replace(array_keys($values), $values, $sql))
            );
            // 返却
            return $response;
        } catch (\PDOException $e) {
            if ($e->errorInfo[0] === '57014') {
                foreach ($values as &$value) {
                    $value = '\'' . $value . '\'';
                }
                throw new TimeoutException(str_replace(array_keys($values), $values, $sql), $e);
            } else {
                throw new Exception('DB実行時エラー : ' . $e->getMessage());
            }
        }
    }

    /**
     * クエリタイプがデータ更新系であるか返却します
     *
     * @param  string $sqlType SQL実行種別
     * @return boolean
     */
    private static function isWriteSql($sqlType)
    {
        switch ($sqlType) {
            case self::SQL_TYPE_SELECT;
                return false;
            case self::SQL_TYPE_INSERT:
            case self::SQL_TYPE_UPDATE;
            case self::SQL_TYPE_DELETE;
                return true;
            default;
                return false;
        }
    }

    /**
     * 連想配列であるかを判定します。
     *
     * @param array $values
     * @return boolean
     */
    private static function isAssoc($values)
    {
        foreach ($values as $key => $value) {
            if (is_int($key) === false) {
                return true;
            }
        }
        return false;
    }

    /**
     * n番目に指定の文字列が現れる開始文字数を取得します。
     *
     * @param  string $value  検索対象文字列
     * @param  string $needle 検索文字列
     * @param  int    $n      n番目指定
     * @return int
     */
    private static function strposN($value, $needle, $n = 0)
    {
        $offset = 0;
        $len = strlen($needle);
        while ($n-- >= 0 && ($pos = strpos($value, $needle, $offset)) !== false) {
            $offset = $pos + $len;
        }
        return $pos;
    }

    /**
     * メッセージ本文内の特殊文字などを削除します。
     *
     * @param  string $sql SQLクエリ
     */
    private static function formatSql(&$sql)
    {
        // コメントアウト削除
        $stepSqls = explode("\n", str_replace(["\r\n", "\r"], "\n", $sql));
        $formatSql = '';
        foreach ($stepSqls as $stepSql) {
            $commentStart = strpos($stepSql, '--');
            if ($commentStart !== false) {
                $formatSql .= substr($stepSql, 0, $commentStart) . ' ';
            } else {
                $formatSql .= $stepSql . ' ';
            }
        }
        // 2文字以上の空白を削除
        $sql = preg_replace('/\s{2,}/', ' ', $formatSql);
    }

    /**
     * SQL・プリペアードステート配列を書き換えます。(タグ形式(:tag)用)
     *
     * @param  string &$sql    SQLクエリ
     * @param  array  &$values プリペアードステート配列
     */
    private static function rewriteSqlForTag(&$sql, &$values)
    {
        foreach ($values as $tag => $value) {
            if (is_array($value) === false) {
                continue;
            }
            $replaceElement = [];
            $replaceSql = '';
            foreach ($value as $elementIndex => $element) {
                $replaceTag = $tag . $elementIndex . '_';
                $replaceSql .= $replaceTag . self::SPLIT_SYMBOL;
                $replaceElement[$replaceTag] = $element;
            }
            // SQL編集
            $sql = str_replace($tag, rtrim($replaceSql, self::SPLIT_SYMBOL), $sql);
            // プリペアードステート配列編集
            unset($values[$tag]);
            $values += $replaceElement;
        }
    }

    /**
     * SQL・プリペアードステート配列を書き換えます。(シンボル形式(?)用)
     *
     * @param  string &$sql    SQLクエリ
     * @param  array  &$values プリペアードステート配列
     */
    private static function rewriteSqlForSymbol(&$sql, &$values)
    {
        /** @var array */
        $replaceValues = [];
        foreach ($values as $index => $value) {
            if (is_array($value) === false) {
                $replaceValues[] = $value;
                continue;
            }
            $replaceSql = rtrim(str_repeat(self::QUESTION_SYMBOL . self::SPLIT_SYMBOL, count($value)), self::SPLIT_SYMBOL);
            $replaceStart = self::strposN($sql, self::QUESTION_SYMBOL, $index);
            // SQL編集
            $sql = substr_replace($sql, $replaceSql, $replaceStart, 1);
            $replaceValues = array_merge($replaceValues, $value);
        }
        // プリペアードステート配列編集
        $values = $replaceValues;
        return;
    }
}
