<?php

namespace Container\Database;

use Illuminate\Support\ServiceProvider;

/**
 * データベース サービスプロバイダー
 *
 * @author tatsuki.kubota
 */
class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('db','Container\Database\PdoMysql');
    }
}
