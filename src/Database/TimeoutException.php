<?php

namespace Container\Database;

use Exception;

/**
 * データベースアクセス タイムアウト 例外クラス
 *
 * @author tatsuki.kubota
 */
class TimeoutException extends Exception
{
    // クラス変数
    protected $sql; // 実行SQL

    /**
     * TimeoutException constructor.
     *
     * @param string    $sql           実行SQL
     * @param string    $preparedState 実行プリペアードステート
     * @param Exception $previous      Exceptionクラス
     */
    public function __construct($sql, Exception $previous = null)
    {
        $this->sql = $sql;
        parent::__construct($previous->getMessage(), $previous->getCode(), $previous);
    }

    /**
     * エラー発生時に実行されていたSQLを返却します。
     *
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }
}
